module.exports = {
  dbUrl: 'mongodb://localhost/todo',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
