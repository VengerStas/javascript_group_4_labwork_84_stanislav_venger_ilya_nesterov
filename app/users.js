const express = require('express');

const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  const user = new User(req.body);
  user.createToken();
  try {
    await user.save();
    return res.send({token: user.token});
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(400).send({error: 'Username incorrect'})
  }

  const isMatch = await user.checkPassword(req.body.password);

  if(!isMatch) {
    return res.status(400).send({error: "Password incorrect"})
  }

  user.createToken();
  await user.save();

  res.send({user: user._id, token: user.token})
});



module.exports = router;