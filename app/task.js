const express = require('express');
const auth = require('../middleware/auth');

const Task = require('../models/Task');

const router = express.Router();

router.post('/', auth, (req, res) => {
 const task = new Task (req.body);

 task.save()
   .then(result => res.send(result))
   .catch(error => res.status(400).send(error))
});

router.get('/', auth, (req, res) => {
 Task.find().populate('user', 'username')
   .then(task => res.send(task))
   .catch(() => res.sendStatus(500))
});

router.put('/:id', auth, (req, res) => {
    Task.findOne({_id: req.params.id, user: req.user._id})
        .then(task => {
        task.title = req.body.title;
        task.description = req.body.description;
        task.status = req.body.status;
        task.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(400));
    })
        .catch(() => res.sendStatus(400));
});

router.delete('/:id', auth, async (req, res) => {
 Task.findByIdAndDelete(req.params.id)
   .then(res.send('task deleted'))
   .catch(() => res.sendStatus(500));
});

module.exports = router;