const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const user = require('./app/users');
const tasks = require('./app/task');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', user);
  app.use('/tasks', tasks);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
