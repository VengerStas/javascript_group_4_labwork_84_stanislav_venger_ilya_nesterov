const mongoose = require('mongoose');
const nanoid = require('nanoid');

const config = require('./config');

const User = require('./models/User');
const Task = require('./models/Task');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for ( let collection of collections) {
    await collection.drop();
  }

  const user = await User.create(
    {username: "Ilya", password: "1ly4", token: nanoid()},
    {username: "John", password: '12345', token: nanoid()}
  );

  await Task.create(
    {user: user[0]._id, title: 'Send message to mother', status: 'complete'},
    {user: user[1]._id, title: 'Pay utility bills', description: 'electric energy: 20 USD, cold water: 10 USD', status: 'new'},
    {user: user[1]._id, title: 'Pay for some food', status: 'in_progress'}
  );

  return connection.close();
};

run().catch(error => {
  console.log(error)
});